AJS.toInit(function($) {
    var PLUGIN_KEY = "com.atlassian.plugins.tutorial.confluence-highlight-actions-demo-plugin:show-matched-text";

    // check if plugin Confluence-highlight-action is enabled
    Confluence.HighlightAction && Confluence.HighlightAction.registerButtonHandler(PLUGIN_KEY, {
        onClick: function(selectionObject) {
            Confluence.HighlightDemoDialogs.showHighlightDemoDialog(selectionObject);
        },
        shouldDisplay: Confluence.HighlightAction.WORKING_AREA.MAINCONTENT_ONLY // this plugin should only affect the main content of a page
    });
});