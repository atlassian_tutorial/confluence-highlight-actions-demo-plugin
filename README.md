# Tutorial: Extending the Highlight Actions Panel

The highlight actions panel in Confluence is the panel that appears when text is selected. 
This tutorial will show you how to extend the highlight actions panel to add a button 
that shows the highlighted text, and the number of times it appears on the page.

For the full tutorial, visit the following URL: [Extending the Highlight Actions Panel][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/display/CONFDEV/Extending+the+Highlight+Actions+Panel
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project